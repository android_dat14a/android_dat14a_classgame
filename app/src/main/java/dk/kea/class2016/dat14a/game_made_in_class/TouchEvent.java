package dk.kea.class2016.dat14a.game_made_in_class;

/**
 * Created by admin on 29/02/16.
 */
public class TouchEvent
{
    public enum TouchEventType
    {
        Down,
        Up,
        Dragged
    }
    public TouchEventType type;
    public int x, y, pointer;
}
