package dk.kea.class2016.dat14a.game_made_in_class;

import android.media.SoundPool;

public class Sound
{
    int soundId;
    SoundPool soundPool;

    public Sound(int soundId, SoundPool soundPool)
    {
        this.soundId = soundId;
        this.soundPool = soundPool;
    }

    public void play(float volume)
    {
        soundPool.play(soundId, volume, volume, 0, 0, 1);
    }
    public void dispose()
    {
        soundPool.unload(soundId);
    }
}