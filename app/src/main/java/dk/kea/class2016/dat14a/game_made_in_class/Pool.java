package dk.kea.class2016.dat14a.game_made_in_class;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 29/02/16.
 */
public abstract class Pool<T>
{
    private List<T> items = new ArrayList<>();

    protected abstract T newItem();

    public T obtain()
    {
        int last = items.size() - 1;
        if(last == -1) return newItem();
        return items.remove(last);
    }

    public void free(T item)
    {
        items.add(item);
    }
}
