package dk.kea.class2016.dat14a.game_made_in_class;

/**
 * Created by admin on 29/02/16.
 */
public class TouchEventPool extends Pool<TouchEvent>
{
    protected TouchEvent newItem()
    {
        return new TouchEvent();
    }
}
