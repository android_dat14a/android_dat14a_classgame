package dk.kea.class2016.dat14a.game_made_in_class;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import java.util.Random;

public class SimpleScreen extends Screen
{
    Bitmap bob;
    float x = -128;
    float y = 0;
    Random rand = new Random();
    int clearColor = Color.CYAN;
    Sound sound;
    Music music;
    boolean userWantsMusic = false;

    public SimpleScreen(Game game)
    {
        super(game);
        bob = game.loadBitmap("bob.png");
        //sound = game.loadSound("explosion.ogg");
        music = game.loadMusic("music.ogg");
    }

    @Override
    public void update(float deltaTime)
    {
        //Log.d("FrameRate", "fps: " + game.getFramerate());
        game.clearFramebuffer(clearColor);
        x = x + 50 * deltaTime;
        if(x > game.getOffscreenWidth()){
            x = -128;
        }
        game.drawBitmap(bob, (int)x, 10);

//        if(game.isTouchDown(0))
//        {
//            if(userWantsMusic)
//            {
//                music.pause();
//                userWantsMusic = false;
//            }
//            else
//            {
//                music.play();
//                userWantsMusic = true;
//            }
//        }
//        for (int pointer = 0; pointer < 5; pointer++)
//        {
//            if(game.isTouchDown(pointer))
//            {
//                game.drawBitmap(bob, game.getTouchX(pointer)-(bob.getWidth()/2), game.getTouchY(pointer)-(bob.getHeight()/2));
//                sound.play(1);
//            }
//        }
//        float x = -game.getAccelerometer()[0];
//        float y = game.getAccelerometer()[1];
//        x = (x/10) * game.getOffscreenWidth()/2 + game.getOffscreenWidth() / 2;
//        y = (y/10) * game.getOffscreenHeight() / 2 + game.getOffscreenHeight() / 2;
//        game.drawBitmap(bob, (int)x-64, (int)y-64);
    }

    @Override
    public void pause()
    {
        music.pause();
        Log.d("SimpleScreen", "We are pausing");
    }

    @Override
    public void resume()
    {
        if(userWantsMusic){music.play();}
        Log.d("SimpleScreen", "We are resuming");
    }

    @Override
    public void dispose()
    {
        music.dispose();
        Log.d("SimpleScreen", "We are disposing the game");
    }
}
