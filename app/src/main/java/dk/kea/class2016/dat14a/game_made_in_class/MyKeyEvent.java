package dk.kea.class2016.dat14a.game_made_in_class;

/**
 * Created by mikjensen on 07/03/16.
 */
public class MyKeyEvent
{
    public enum KeyEventType
    {
        Down,
        Up
    }
    public KeyEventType type;
    public int KeyCode;
    public char character;
}
