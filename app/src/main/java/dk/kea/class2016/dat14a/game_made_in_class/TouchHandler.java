package dk.kea.class2016.dat14a.game_made_in_class;

/**
 * Created by admin on 29/02/16.
 */
public interface TouchHandler
{
    boolean isTouchDown(int pointer);
    int getTouchX(int pointer);
    int getTouchY(int pointer);
}
