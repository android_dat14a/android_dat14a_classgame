package dk.kea.class2016.dat14a.game_made_in_class;

public enum State
{
    Running,
    Paused,
    Resumed,
    Disposed
}
