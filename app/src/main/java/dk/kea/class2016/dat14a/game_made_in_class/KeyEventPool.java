package dk.kea.class2016.dat14a.game_made_in_class;

public class KeyEventPool extends Pool<MyKeyEvent>
{
    @Override
    protected MyKeyEvent newItem()
    {
        return new MyKeyEvent();
    }
}
